//We can't use int, because sometimes we are storing values that are far greater than the ones allowed by int (which is 2^19 [512k])
//#define USE_INT

using System;

namespace DeterministicSimulation
{
	//Fixed point math library taken from:
	//http://stackoverflow.com/questions/605124/fixed-point-math-in-c
	//Removed some explicit conversion and operations that allowed "int" parameters instead of fint (better control over involuntary conversions)
    [Serializable]
	public struct fint
	{
#if USE_INT
		public int raw;
#else
		public long raw;
#endif

		public const int SHIFT_AMOUNT = 12; //12 is 4096
		
		private const long rawOne = 1 << SHIFT_AMOUNT;

		public static fint zero = fint.CreateFromInt( 0 );

		public static fint one = fint.CreateFromInt( 1 );
		public static fint two = fint.CreateFromInt( 2 );
        public static fint three = fint.CreateFromInt(3);
		public static fint hundred = fint.CreateFromInt( 100 );

		public static fint half = fint.CreateFromInt( 1 ) / fint.CreateFromInt( 2 );
		public static fint quarter = fint.CreateFromInt( 1 ) / fint.CreateFromInt( 4 );

        public static fint step = fint.CreateRaw(1);

#if USE_INT
        public static fint MinValue = CreateRaw(int.MinValue);
        public static fint MaxValue = CreateRaw(int.MaxValue);
#else
        public static fint MinValue = CreateRaw(long.MinValue);
        public static fint MaxValue = CreateRaw(long.MaxValue);
#endif

		#region Constructors

#if USE_INT
		public static fint CreateRaw(int StartingRawValue)
#else
		public static fint CreateRaw(long StartingRawValue)
#endif
		{
			fint fInt;
			fInt.raw = StartingRawValue;
			return fInt;
		}

        public static fint CreateFromString( string StringValue )
        {
            long intPart = 0;

            long decPart = 0;
            long decDivisor = 1;

            bool inDecimals = false;
            bool negative = false;

            for (int i = 0; i < StringValue.Length; i++)
            {
                char c = StringValue[i];
                switch (c)
                {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        if (inDecimals)
                        {
                            long newDecPart = (decPart * 10L) + (long)(c - '0');
                            if (newDecPart < decPart)
                                throw new FormatException("Invalid fint format (overflow): " + StringValue);
                            decPart = newDecPart;
                            decDivisor *= 10;
                        }
                        else
                        {
                            long newIntPart = (intPart * 10L) + (long)(c - '0');
                            if (newIntPart < intPart)
                                throw new FormatException("Invalid fint format (overflow): " + StringValue);
                            intPart = newIntPart;
                        }
                        break;

                    case '-':
                        if (i == 0)
                            negative = true;
                        else
                            throw new FormatException("Invalid fint format: " + StringValue);
                        break;
                    
                    case '+':
                        if (i == 0)
                            negative = false;
                        else
                            throw new FormatException("Invalid fint format: " + StringValue);
                        break;
                    
                    case '.':
                        if (!inDecimals)
                            inDecimals = true;
                        else
                            throw new FormatException("Invalid fint format: " + StringValue);
                        break;
                }
            }

            fint fInt;
            fInt.raw = intPart;
            fInt.raw = fInt.raw << SHIFT_AMOUNT;
            fInt.raw = fInt.raw | ((decPart * (1L << SHIFT_AMOUNT)) / decDivisor); //This is magic, we spent 2 hs with mariano coming up with this solution
            if (negative)
                fInt.raw = -fInt.raw;

            return fInt;
        }

		public static fint CreateFromInt( int IntValue )
		{
			fint fInt;
			fInt.raw = IntValue;
			fInt.raw = fInt.raw << SHIFT_AMOUNT;
			return fInt;
		}

        public static fint CreateFromFraction(int numerator, int denominator)
        {
            return fint.CreateFromInt(numerator) / fint.CreateFromInt(denominator);
        }

        public static fint CreateFromBase100(int numberBase100)
        {
            return CreateFromFraction(numberBase100, 100);
        }

        /// <summary>
        /// WARNING!!! THIS IS NOT-DETERMINISTIC!!!! DEPENDS ON PLATFORM FLOAT IMPLEMENTATION
        /// </summary>
		public static fint CreateFromFloat_Warning( float FloatValue )
		{
			fint fInt;
			FloatValue *= (float)rawOne;
			fInt.raw = (int)UnityEngine.Mathf.Round( FloatValue );
			return fInt;
		}

		#endregion
		
		public int ToInt()
		{
			return (int)( this.raw >> SHIFT_AMOUNT );
		}
		
        public long ToLong()
        {
            return (long)( this.raw >> SHIFT_AMOUNT );
        }

		public float ToFloat()
		{
			return (float)this.raw / (float) rawOne;
		}

        public fint Ceil()
        {
            return CreateRaw((this.raw + (rawOne - 1)) & ~(rawOne - 1));
        }
		
        public fint Floor()
        {
            return CreateRaw(raw & ~(rawOne - 1));
        }

        public fint Round()
        {
            long decPart = this.raw & (rawOne - 1);

            long floor = Floor().raw;
            bool even = (ToInt() % 2) == 0;

            if (decPart == half.raw)
                return fint.CreateRaw(floor + (even ? fint.zero.raw : fint.one.raw));
            else if (decPart == -half.raw)
                return fint.CreateRaw(floor - (even ? fint.zero.raw : fint.one.raw));
            else if (decPart > half.raw || decPart < -half.raw)
                return Ceil();
            else
                return Floor();
        }

		#region FromParts
		/// <summary>
		/// Create a fixed-int number from parts.  For example, to create 1.5 pass in 1 and 500.
		/// </summary>
		/// <param name="PreDecimal">The number above the decimal.  For 1.5, this would be 1.</param>
		/// <param name="PostDecimal">The number below the decimal, to three digits.  
		/// For 1.5, this would be 500. For 1.005, this would be 5.</param>
		/// <returns>A fixed-int representation of the number parts</returns>
		public static fint FromParts( int PreDecimal, int PostDecimal )
		{
			fint f = fint.CreateFromInt( PreDecimal );
			if ( PostDecimal != 0 )
				f += ( fint.CreateFromInt( PostDecimal ) / fint.CreateFromInt( 1000 ));
			return f;
		}
		#endregion
		
		#region *
		public static fint operator *( fint first, fint second )
		{
			fint fInt;
#if USE_INT
            long longRaw = (((long) first.raw) * ((long) second.raw)) >> SHIFT_AMOUNT;

            if (longRaw > int.MaxValue)
            {
                fInt = fint.MaxValue;

                #if UNITY_EDITOR
                UnityEngine.Debug.LogError(string.Format("Overflow: {0} x {1} overflows in fint", first, second));
                #endif
            }
            else
            {
                fInt.raw = (int) longRaw;
            }
#else
			fInt.raw = ( first.raw * second.raw ) >> SHIFT_AMOUNT;
#endif

			return fInt;
		}
		#endregion
		
		#region /
		public static fint operator /( fint one, fint other )
		{
			fint fInt;
#if USE_INT
			fInt.raw = (int) ( ( ((long) one.raw) << SHIFT_AMOUNT ) / ((long) other.raw) );
#else
			fInt.raw = ( one.raw << SHIFT_AMOUNT ) / ( other.raw );
#endif
			return fInt;
		}
		#endregion
		
		#region %
		public static fint operator %( fint one, fint divisor )
		{
			fint fInt;
			
            fInt.raw = ( one.raw ) % ( divisor.raw );
			
            return fInt;
		}
		#endregion
		
		#region +
		public static fint operator +( fint one, fint other )
		{
			fint fInt;
            fInt.raw = one.raw + other.raw;
			return fInt;
		}
		#endregion
		
		#region -
		public static fint operator -( fint one, fint other )
		{
			fint fInt;
			fInt.raw = one.raw - other.raw;
			return fInt;
		}
		#endregion
		
		#region ==
		public static bool operator ==( fint one, fint other )
		{
			return one.raw == other.raw;
		}
		#endregion
		
		#region !=
		public static bool operator !=( fint one, fint other )
		{
			return one.raw != other.raw;
		}
		#endregion
		
		#region >=
		public static bool operator >=( fint one, fint other )
		{
			return one.raw >= other.raw;
		}
		#endregion
		
		#region <=
		public static bool operator <=( fint one, fint other )
		{
			return one.raw <= other.raw;
		}
		#endregion
		
		#region >
		public static bool operator >( fint one, fint other )
		{
			return one.raw > other.raw;
		}
		#endregion
		
		#region <
		public static bool operator <( fint one, fint other )
		{
			return one.raw < other.raw;
		}
		#endregion

		public static fint operator <<( fint one, int Amount )
		{
			return fint.CreateRaw( one.raw << Amount );
		}
		
		public static fint operator >>( fint one, int Amount )
		{
			return fint.CreateRaw( one.raw >> Amount );
		}

		public static fint operator -(fint a)
		{
			a.raw = -a.raw;

			return a;
		}

        public static fint operator ^( fint one, long other )
        {
            fint fInt;
            fInt.raw = one.raw ^ other;
            return fInt;
        }
		
		public override bool Equals( object obj )
		{
			if ( obj is fint )
				return ( (fint)obj ).raw == this.raw;
			else
				return false;
		}
		
		public override int GetHashCode()
		{
			return raw.GetHashCode();
		}
		
		public override string ToString()
		{
			return this.ToFloat().ToString();
		}
	}
}

