﻿using System;
using DeterministicSimulation;
using Entitas.VisualDebugging.Unity.Editor;
using UnityEditor;

namespace TypeDrawer
{
    public class LongTypeDrawer : ITypeDrawer
    {
        public bool HandlesType(Type type)
        {
            return type == typeof(long);
        }

        public object DrawAndGetNewValue(Type memberType, string memberName, object value, object target)
        {
            var longValue = (long) value;
            longValue = EditorGUILayout.LongField(memberName, longValue);
            return longValue;
        }
    }
}
