﻿using System;
using DeterministicSimulation;
using Entitas.VisualDebugging.Unity.Editor;
using UnityEditor;

namespace TypeDrawer
{
    public class FintTypeDrawer : ITypeDrawer
    {
        public bool HandlesType(Type type)
        {
            return type == typeof(fint);
        }

        public object DrawAndGetNewValue(Type memberType, string memberName, object value, object target)
        {
            var floatInt = (fint) value;
            floatInt = fint.CreateFromFloat_Warning(EditorGUILayout.FloatField(memberName, floatInt.ToFloat()));
            return floatInt;
        }
    }
}
