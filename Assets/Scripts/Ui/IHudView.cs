﻿namespace Ui
{
    public interface IHudView
    {
        void SetHealth(int value, int max);
        void HideHealthBar();
        void ShowHealthBar();
    }
}
