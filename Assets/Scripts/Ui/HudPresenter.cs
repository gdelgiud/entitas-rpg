﻿using Entitas;

namespace Ui
{
    public class HudPresenter
    {
        private readonly IGroup<GameEntity> _controllableEntities;
        private readonly IGroup<GameEntity> _deadControllableEntities;
        private IHudView _view;

        public HudPresenter(IHudView view, Contexts contexts)
        {
            _view = view;
            var gameContext = contexts.game;
            _controllableEntities = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Health, GameMatcher.Controllable));
            _deadControllableEntities = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Dead, GameMatcher.Controllable));
            Subscribe();
            ShowInitialEntityHealth();
        }

        private void Subscribe()
        {
            _controllableEntities.OnEntityAdded += OnHealthChanged;
            _deadControllableEntities.OnEntityAdded += OnTargetDeath;
        }

        private void Unsubscribe()
        {
            _controllableEntities.OnEntityAdded -= OnHealthChanged;
            _deadControllableEntities.OnEntityAdded -= OnTargetDeath;
        }

        private void OnTargetDeath(IGroup<GameEntity> group, GameEntity entity, int index, IComponent component)
        {
            _view.HideHealthBar();
        }

        private void OnHealthChanged(IGroup<GameEntity> group, GameEntity entity, int index, IComponent component)
        {
            ShowEntityHealth(entity);
        }

        private void ShowEntityHealth(GameEntity entity)
        {
            var healthComponent = entity.health;
            _view.ShowHealthBar();
            _view.SetHealth(healthComponent.Value, healthComponent.Max);
        }

        private void ShowInitialEntityHealth()
        {
            if (_controllableEntities.count > 0)
            {
                ShowEntityHealth(_controllableEntities.GetEntities()[0]);
            }
            else
            {
                _view.HideHealthBar();
            }
        }

        public void Detach()
        {
            _view = null;
            Unsubscribe();
        }
    }
}
