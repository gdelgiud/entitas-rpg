﻿using UnityEngine;
using UnityEngine.UI;

namespace Ui
{
    public class HudView : MonoBehaviour, IHudView
    {
        public Text HealthLabel;
        public Slider HealthBar;
        
        private HudPresenter _hudPresenter;

        private void Start()
        {
            _hudPresenter = new HudPresenter(this, Contexts.sharedInstance);
        }

        private void OnDestroy()
        {
            _hudPresenter.Detach();
        }

        public void SetHealth(int value, int max)
        {
            HealthBar.value = (float) value / max;
        }

        public void HideHealthBar()
        {
            HealthBar.gameObject.SetActive(false);
            HealthLabel.gameObject.SetActive(false);
        }

        public void ShowHealthBar()
        {
            HealthBar.gameObject.SetActive(true);
            HealthLabel.gameObject.SetActive(true);
        }
    }
}
