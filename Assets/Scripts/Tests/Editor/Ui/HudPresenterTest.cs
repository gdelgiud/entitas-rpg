﻿using NSubstitute;
using NUnit.Framework;

namespace Ui
{
    [TestFixture]
    public class HudPresenterTest
    {
        private HudPresenter _presenter;
        private IHudView _hudView;
        private GameContext _gameContext;

        [SetUp]
        public void Setup()
        {
            var contexts = new Contexts();
            _gameContext = contexts.game;
            _hudView = Substitute.For<IHudView>();
            _presenter = new HudPresenter(_hudView, contexts);
        }
        
        [Test]
        public void WhenControllableEntityHealthIsUpdatedThenUpdateHealthBar()
        {
            var entity = _gameContext.CreateEntity();
            entity.isControllable = true;
            entity.AddHealth(30, 100);
            
            _hudView.Received().ShowHealthBar();
            _hudView.Received().SetHealth(30, 100);
        }

        [Test]
        public void WhenControllableEntityDiesThenHidesHealthBar()
        {
            var entity = _gameContext.CreateEntity();
            entity.isControllable = true;
            entity.AddHealth(30, 100);
            entity.isDead = true;
            
            _hudView.Received().HideHealthBar();
        }

        [Test]
        public void WhenCreatedWithNoControllableEntitiesThenHidesHealthBar()
        {
            _hudView.Received().HideHealthBar();
        }
    }
}
