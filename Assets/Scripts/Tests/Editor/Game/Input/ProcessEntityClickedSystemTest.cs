﻿using Game.Input.Damage;
using NUnit.Framework;

namespace Game.Input
{
    [TestFixture]
    public class ProcessEntityClickedSystemTest
    {
        private const int ExistingId = 3;
        private const int NonExistingId = 5;
        private GameContext _gameContext;
        private ProcessEntityClickedSystem _system;
        private GameEntity _targetEntity;
        private InputEntity _clickEntity;

        [SetUp]
        public void Setup()
        {
            var contexts = new Contexts();
            var inputContext = contexts.input;
            _gameContext = contexts.game;
            _system = new ProcessEntityClickedSystem(contexts);
            _targetEntity = _gameContext.CreateEntity();
            _clickEntity = inputContext.CreateEntity();
        }

        [Test]
        public void Cleanup()
        {
            _targetEntity.isDestroyed = true;
        }

        [Test]
        public void WhenClickedEntityExistsThenCreatesDamageAction()
        {
            GivenTargetEntity();
            GivenExistingEntityClickedInput();
            
            WhenExecute();

            AssertCorrectDamageActionCreated();
        }

        [Test]
        public void WhenClickedEntityDoesNotExistThenDamageActionIsNotCreated()
        {
            GivenTargetEntity();
            GivenNonExistingEntityClickedInput();
            
            WhenExecute();

            AssertDamageActionNotCreated();
        }

        private void AssertDamageActionNotCreated()
        {
            var damageActions = _gameContext.GetGroup(GameMatcher.DamageAction);
            Assert.IsEmpty(damageActions.GetEntities());
        }

        private void GivenTargetEntity()
        {
            _targetEntity.AddId(ExistingId);
        }

        private void GivenExistingEntityClickedInput()
        {
            _clickEntity.AddEntityClickedInput(ExistingId);
        }

        private void GivenNonExistingEntityClickedInput()
        {
            _clickEntity.AddEntityClickedInput(NonExistingId);
        }

        private void WhenExecute()
        {
            _system.Execute();
        }

        private void AssertCorrectDamageActionCreated()
        {
            var damageActions = _gameContext.GetGroup(GameMatcher.DamageAction);
            Assert.AreEqual(ExistingId, damageActions.GetSingleEntity().damageAction.TargetId);
        }
    }
}
