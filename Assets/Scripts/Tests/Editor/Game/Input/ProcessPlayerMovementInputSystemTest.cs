﻿using DeterministicSimulation;
using Game.Input.Player;
using NUnit.Framework;

namespace Game.Input
{
    [TestFixture]
    public class ProcessPlayerMovementInputSystemTest
    {
        private InputContext _inputContext;
        private ProcessPlayerMovementInputSystem _system;
        private GameEntity _entity;

        [SetUp]
        public void Setup()
        {
            var contexts = new Contexts();
            _inputContext = contexts.input;
            var gameContext = contexts.game;
            _system = new ProcessPlayerMovementInputSystem(contexts);
            _entity = gameContext.CreateEntity();
        }
        
        [Test]
        public void WhenInputExistsThenReplaceControllableEntityVelocity()
        {
            var xAxis = fint.one;
            var yAxis = fint.half;
            GivenControllableEntity();
            GivenPlayerMovementInput(xAxis, yAxis);
            
            WhenExecute();
            
            AssertEntityHasVelocity(xAxis, yAxis);
        }

        [Test]
        public void WhenInputExistsThenNonControllableEntityIsNotAffected()
        {
            var xVelocity = fint.CreateFromInt(4);
            var yVelocity = fint.CreateFromInt(7);
            GivenNonControllableEntity();
            GivenEntityWithVelocity(xVelocity, yVelocity);
            GivenPlayerMovementInput(fint.one, fint.half);
            
            WhenExecute();
            
            AssertEntityHasVelocity(xVelocity, yVelocity);
        }

        [Test]
        public void WhenInputDoesNotExistThenRemovesVelocityFromControllableEntity()
        {
            var xVelocity = fint.CreateFromInt(4);
            var yVelocity = fint.CreateFromInt(7);
            GivenControllableEntity();
            GivenEntityWithVelocity(xVelocity, yVelocity);
            GivenNoPlayerMovementInput();
            
            WhenExecute();
            
            AssertEntityHasNoVelocity();
        }

        [Test]
        public void WhenInputDoesNotExistThenNonControllableEntityIsNotAffected()
        {
            var xVelocity = fint.CreateFromInt(4);
            var yVelocity = fint.CreateFromInt(7);
            GivenNonControllableEntity();
            GivenEntityWithVelocity(xVelocity, yVelocity);
            GivenNonControllableEntity();
            
            WhenExecute();
            
            AssertEntityHasVelocity(xVelocity, yVelocity);
        }

        private void GivenControllableEntity()
        {
            _entity.isControllable = true;
        }

        private void GivenNonControllableEntity()
        {
            _entity.isControllable = false;
        }

        private void GivenEntityWithVelocity(fint xVelocity, fint yVelocity)
        {
            _entity.ReplaceVelocity(xVelocity, yVelocity);
        }

        private void GivenPlayerMovementInput(fint xAxis, fint yAxis)
        {
            _inputContext.SetPlayerMovement(xAxis, yAxis);
        }

        private void GivenNoPlayerMovementInput()
        {
            if (_inputContext.hasPlayerMovement)
            {
                _inputContext.RemovePlayerMovement();
            }
        }

        private void WhenExecute()
        {
            _system.Execute();
        }

        private void AssertEntityHasVelocity(fint xVelocity, fint yVelocity)
        {
            Assert.IsTrue(_entity.hasVelocity);
            Assert.AreEqual(xVelocity, _entity.velocity.X);
            Assert.AreEqual(yVelocity, _entity.velocity.Y);
        }

        private void AssertEntityHasNoVelocity()
        {
            Assert.IsFalse(_entity.hasVelocity);
        }
    }
}
