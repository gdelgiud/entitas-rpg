﻿using Game.Logic.Health;
using NUnit.Framework;

namespace Game.Logic
{
    [TestFixture]
    public class ApplyDamageSystemTest
    {
        private const int MaxHealth = 100;
        private const long ExistingId = 3;
        private const long NonExistingId = 5;
        
        private GameContext _gameContext;
        private ApplyDamageSystem _system;
        private GameEntity _gameEntity;
        private GameEntity _actionEntity;

        [SetUp]
        public void Setup()
        {
            var contexts = new Contexts();
            _gameContext = contexts.game;
            _system = new ApplyDamageSystem(contexts);
            _gameEntity = _gameContext.CreateEntity();
            _actionEntity = _gameContext.CreateEntity();
        }

        [TearDown]
        public void Cleanup()
        {
            Assert.IsTrue(_actionEntity.isDestroyed);
        }
        
        [Test]
        public void WhenEntityExistsAndHasHealthThenAppliesDamage()
        {
            GivenTargetWithHealth(30);
            GivenDamage(10);

            WhenExecute();
            
            AssertTargetWasDamaged();
            AssertTargetHasHealth(20);
        }

        [Test]
        public void WhenEntityExistsAndHasNoHealthThenDoesNothing()
        {
            GivenTargetWithNoHealth();
            GivenDamage(10);
            
            WhenExecute();
            
            AssertTargetWasNotDamaged();
            AssertTargetHasNoHealth();
        }

        [Test]
        public void WhenEntityDoesNotExistThenDoesNothing()
        {
            GivenTargetWithHealth(30);
            GivenDamageForNonExistingEntity(10);
            
            WhenExecute();
            
            AssertTargetWasNotDamaged();
            AssertTargetHasHealth(30);
        }

        [Test]
        public void WhenDamageHasNegativeAmountThenDoesNothing()
        {
            GivenTargetWithHealth(30);
            GivenDamage(-10);
            
            WhenExecute();
            
            AssertTargetWasNotDamaged();
            AssertTargetHasHealth(30);
        }

        private void GivenTargetWithHealth(int health)
        {
            _gameEntity.AddId(ExistingId);
            _gameEntity.AddHealth(health, MaxHealth);
        }

        private void GivenTargetWithNoHealth()
        {
            _gameEntity.AddId(ExistingId);
            if (_gameEntity.hasHealth)
                _gameEntity.RemoveHealth();
        }

        private void GivenDamage(int amount)
        {
            _actionEntity.AddDamageAction(ExistingId, amount);
        }

        private void GivenDamageForNonExistingEntity(int amount)
        {
            _actionEntity.AddDamageAction(NonExistingId, amount);
        }

        private void WhenExecute()
        {
            _system.Execute();
        }

        private void AssertTargetHasHealth(int health)
        {
            Assert.IsTrue(_gameEntity.hasHealth);
            Assert.AreEqual(health, _gameEntity.health.Value);
        }

        private void AssertTargetHasNoHealth()
        {
            Assert.IsFalse(_gameEntity.hasHealth);
        }

        private void AssertTargetWasDamaged()
        {
            Assert.IsTrue(_gameEntity.isDamaged);
        }

        private void AssertTargetWasNotDamaged()
        {
            Assert.IsFalse(_gameEntity.isDamaged);
        }
    }
}
