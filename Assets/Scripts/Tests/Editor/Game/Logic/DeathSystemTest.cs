﻿using Game.Logic.Health;
using NUnit.Framework;

namespace Game.Logic
{
    [TestFixture]
    public class DeathSystemTest
    {
        private DeathSystem _system;
        private GameEntity _entity;

        [SetUp]
        public void Setup()
        {
            var contexts = new Contexts();
            var gameContext = contexts.game;
            _system = new DeathSystem(contexts);
            _entity = gameContext.CreateEntity();
        }

        [Test]
        public void WhenEntityHealthReachesZeroThenEntityIsSetAsDead()
        {
            _entity.AddHealth(0, 100);
            
            _system.Execute();
            
            Assert.IsTrue(_entity.isDead);
        }

        [Test]
        public void WhenEntityHealthIsGreaterThanZeroThenEntityIsNotSetAsDead()
        {
            _entity.AddHealth(30, 100);
            
            _system.Execute();
            
            Assert.IsFalse(_entity.isDead);
        }

        [Test]
        public void WhenEntityHealthIsGreaterThanZeroAndEntityIsAlreadyDeadThenEntityRemainsDead()
        {
            _entity.AddHealth(30, 100);
            _entity.isDead = true;
            
            _system.Execute();
            
            Assert.IsTrue(_entity.isDead);
        }
    }
}
