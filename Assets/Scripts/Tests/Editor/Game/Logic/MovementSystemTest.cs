﻿using DeterministicSimulation;
using Game.Logic.Movement;
using NUnit.Framework;

namespace Game.Logic
{
    [TestFixture]
    public class MovementSystemTest
    {
        private MovementSystem _system;
        private Contexts _contexts;
        private GameEntity _entity;

        [SetUp]
        public void Setup()
        {
            _contexts = new Contexts();
            _system = new MovementSystem(_contexts);
            _entity = _contexts.game.CreateEntity();
        }

        [Test]
        public void WhenEntityHasVelocityEntityIsMoved()
        {
            var xVelocity = fint.CreateFromInt(1);
            var yVelocity = fint.CreateFromInt(2);
            var xPosition = fint.CreateFromInt(3);
            var yPosition = fint.CreateFromInt(4);
            var step = fint.CreateFromInt(1);
            _entity.AddPosition(xPosition, yPosition);
            _entity.AddVelocity(xVelocity, yVelocity);
            _contexts.input.SetDeltaTime(step);

            _system.Execute();

            Assert.AreEqual(fint.CreateFromInt(4), _entity.position.X, "X Position");
            Assert.AreEqual(fint.CreateFromInt(6), _entity.position.Y, "Y Position");
        }

        [Test]
        public void WhenEntityDoesNotHaveVelocityEntityIsNotMoved()
        {
            var xPosition = fint.CreateFromInt(3);
            var yPosition = fint.CreateFromInt(4);
            var step = fint.CreateFromInt(1);
            _entity.AddPosition(xPosition, yPosition);
            _contexts.input.SetDeltaTime(step);

            _system.Execute();

            Assert.AreEqual(xPosition, _entity.position.X, "X Position");
            Assert.AreEqual(yPosition, _entity.position.Y, "Y Position");
        }
    }
}
