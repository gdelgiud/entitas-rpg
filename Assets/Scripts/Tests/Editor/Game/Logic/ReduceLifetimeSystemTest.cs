﻿using DeterministicSimulation;
using Game.Logic.Lifetime;
using NUnit.Framework;

namespace Game.Logic
{
    [TestFixture]
    public class ReduceLifetimeSystemTest
    {
        private GameContext _gameContext;
        private ReduceLifetimeSystem _system;
        private GameEntity _lifetimeEntity;
        private InputEntity _deltaTimeEntity;

        [SetUp]
        public void Setup()
        {
            var contexts = new Contexts();
            _gameContext = contexts.game;
            var inputContext = contexts.input;
            _system = new ReduceLifetimeSystem(contexts);
            _lifetimeEntity = _gameContext.CreateEntity();
            _deltaTimeEntity = inputContext.CreateEntity();
        }
        
        [Test]
        public void WhenAppliesDeltaTimeAndLifetimeReachesZeroThenFlagsEntityAsDestroyed()
        {
            GivenLifetime(1);
            GivenDeltaTime(2);
            
            WhenExecute();
            
            AssertLifetimeEntityDestroyed();
        }

        [Test]
        public void WhenAppliesDeltaTimeAndLifetimeIsGreaterThanZeroThenReducesLifetime()
        {
            GivenLifetime(3);
            GivenDeltaTime(1);
            
            WhenExecute();
            
            AssertLifetimeEntityNotDestroyed();
            AssertLifetimeIs(2);
        }

        private void GivenLifetime(int lifetime)
        {
            _lifetimeEntity.AddLifetime(fint.CreateFromInt(lifetime));
        }

        private void GivenDeltaTime(int deltaTime)
        {
            _deltaTimeEntity.AddDeltaTime(fint.CreateFromInt(deltaTime));
        }

        private void WhenExecute()
        {
            _system.Execute();
        }

        private void AssertLifetimeEntityNotDestroyed()
        {
            Assert.IsFalse(_lifetimeEntity.isDestroyed);
        }

        private void AssertLifetimeEntityDestroyed()
        {
            Assert.IsTrue(_lifetimeEntity.isDestroyed);
        }

        private void AssertLifetimeIs(int lifetime)
        {
            Assert.AreEqual(fint.CreateFromInt(lifetime), _lifetimeEntity.lifetime.Value);
        }
    }
}
