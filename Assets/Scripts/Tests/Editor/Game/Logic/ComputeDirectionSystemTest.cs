﻿using DeterministicSimulation;
using Game.Logic.Movement;
using NUnit.Framework;

namespace Game.Logic
{
    [TestFixture]
    public class ComputeDirectionSystemTest
    {
        private GameContext _gameContext;
        private ComputeDirectionSystem _system;
        private GameEntity _entity;

        [SetUp]
        public void Setup()
        {
            var contexts = new Contexts();
            _gameContext = contexts.game;
            _system = new ComputeDirectionSystem(contexts);
            _entity = _gameContext.CreateEntity();
        }

        [Test]
        public void WhenEntityHasGreaterNegativeXVelocityThenDirectionIsLeft()
        {
            GivenVelocities(-5, -3);

            WhenExecute();

            ThenDirectionIs(Direction.Left);
        }

        [Test]
        public void WhenEntityHasGreaterPositiveXVelocityThenDirectionIsRight()
        {
            GivenVelocities(5, 3);

            WhenExecute();

            ThenDirectionIs(Direction.Right);
        }

        [Test]
        public void WhenEntityHasGreaterNegativeYVelocityThenDirectionIsDown()
        {
            GivenVelocities(-3, -5);

            WhenExecute();

            ThenDirectionIs(Direction.Down);
        }

        [Test]
        public void WhenEntityHasGreaterPositiveYVelocityThenDirectionIsUp()
        {
            GivenVelocities(3, 5);

            WhenExecute();

            ThenDirectionIs(Direction.Up);
        }

        [Test]
        public void WhenEntityHasEqualNegativeXVelocityAndPositiveYVelocityThenDirectionIsUp()
        {
            GivenVelocities(-3, 3);

            WhenExecute();

            ThenDirectionIs(Direction.Up);
        }

        [Test]
        public void WhenEntityHasEqualNegativeXVelocityAndNegativeYVelocityThenDirectionIsDown()
        {
            GivenVelocities(-3, -3);

            WhenExecute();

            ThenDirectionIs(Direction.Down);
        }

        [Test]
        public void WhenEntityHasEqualPositiveXVelocityAndNegativeYVelocityThenDirectionIsDown()
        {
            GivenVelocities(3, -3);

            WhenExecute();

            ThenDirectionIs(Direction.Down);
        }

        [Test]
        public void WhenEntityHasEqualPositiveXVelocityAndNegativeYVelocityThenDirectionIsUp()
        {
            GivenVelocities(3, 3);

            WhenExecute();

            ThenDirectionIs(Direction.Up);
        }

        [Test]
        public void WhenEntityIsNotMovingAndAlreadyHasDirectionThenDirectionIsSameAsBefore()
        {
            const Direction previousDirection = Direction.Left;
            GivenDirection(previousDirection);
            GivenNotMovingEntity();

            WhenExecute();

            ThenDirectionIs(previousDirection);
        }

        [Test]
        public void WhenEntityIsNotMovingAndHasNoPreviousDirectionThenDirectionIsDown()
        {
            GivenNoPreviousDirection();
            GivenNotMovingEntity();
            
            WhenExecute();
            
            ThenDirectionIs(Direction.Down);
        }

        private void GivenVelocities(int xVelocity, int yVelocity)
        {
            _entity.AddVelocity(fint.CreateFromInt(xVelocity), fint.CreateFromInt(yVelocity));
        }

        private void GivenNotMovingEntity()
        {
            GivenVelocities(0, 0);
        }

        private void GivenDirection(Direction direction)
        {
            _entity.AddDirection(direction);
        }

        private void GivenNoPreviousDirection()
        {
            if (_entity.hasDirection)
            {
                _entity.RemoveDirection();
            }
        }

        private void WhenExecute()
        {
            _system.Execute();
        }

        private void ThenDirectionIs(Direction direction)
        {
            Assert.IsTrue(_entity.hasDirection);
            Assert.AreEqual(direction, _entity.direction.Value);
        }
    }
}
