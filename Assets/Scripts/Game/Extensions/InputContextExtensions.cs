﻿namespace Game.Extensions
{
    public static class InputContextExtensions
    {
        public static InputEntity OnEntityClicked(this InputContext inputContext, GameEntity clickedEntity)
        {
            var clickInputEntity = inputContext.CreateEntity();
            clickInputEntity.AddEntityClickedInput(clickedEntity.id.Id);
            return clickInputEntity;
        }
    }
}
