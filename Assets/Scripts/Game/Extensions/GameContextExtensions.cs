﻿using DeterministicSimulation;

namespace Game.Extensions
{
    public static class GameContextExtensions
    {
        public static GameEntity CreateDamageAction(this GameContext gameContext, GameEntity targetEntity, int amount)
        {
            var entity = gameContext.CreateEntity();
            entity.AddDamageAction(targetEntity.id.Id, amount);
            return entity;
        }

        public static GameEntity CreateEffectOnEntity(this GameContext gameContext, GameEntity targetEntity, string effectName)
        {
            var effectEntity = gameContext.CreateEntity();
            effectEntity.AddAsset(effectName);
            effectEntity.AddPosition(targetEntity.position.X, targetEntity.position.Y);
            effectEntity.AddLifetime(fint.one);
            return effectEntity;
        }

        public static GameEntity CreateCharacter(this GameContext gameContext, string assetName, fint xPosition, fint yPosition, int health)
        {
            var character = gameContext.CreateEntity();
            character.AddAsset(assetName);
            character.AddPosition(xPosition, yPosition);
            character.AddHealth(health, health);
            return character;
        }

        public static GameEntity CreateTree(this GameContext gameContext, fint xPosition, fint yPosition)
        {
            var tree = gameContext.CreateEntity();
            tree.AddAsset("Tree");
            tree.AddPosition(xPosition, yPosition);
            return tree;
        }
    }
}
