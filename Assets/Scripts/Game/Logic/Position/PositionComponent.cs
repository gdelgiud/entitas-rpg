﻿using DeterministicSimulation;
using Entitas;

namespace Game.Logic.Position
{
    [Game]
    public sealed class PositionComponent : IComponent
    {
        public fint X;
        public fint Y;
    }
}
