﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Game.Logic.Id
{
    [Game]
    public sealed class IdComponent : IComponent
    {
        [PrimaryEntityIndex] public long Id;
    }
}
