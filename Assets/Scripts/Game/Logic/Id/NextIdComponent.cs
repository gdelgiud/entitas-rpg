﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Game.Logic.Id
{
    [Game]
    [Unique]
    public sealed class NextIdComponent : IComponent
    {
        public long Value;
    }
}
