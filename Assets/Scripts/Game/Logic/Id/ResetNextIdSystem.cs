﻿using Entitas;

namespace Game.Logic.Id
{
    public class ResetNextIdSystem : IInitializeSystem
    {
        private readonly GameContext _gameContext;
        
        public ResetNextIdSystem(Contexts contexts)
        {
            _gameContext = contexts.game;
        }

        public void Initialize()
        {
            _gameContext.ReplaceNextId(0L);
        }
    }
}
