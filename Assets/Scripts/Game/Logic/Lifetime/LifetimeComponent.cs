﻿using DeterministicSimulation;
using Entitas;

namespace Game.Logic.Lifetime
{
    [Game]
    public sealed class LifetimeComponent : IComponent
    {
        public fint Value;
    }
}
