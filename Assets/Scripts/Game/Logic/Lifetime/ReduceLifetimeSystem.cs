﻿using System.Collections.Generic;
using DeterministicSimulation;
using Entitas;

namespace Game.Logic.Lifetime
{
    public class ReduceLifetimeSystem : ReactiveSystem<InputEntity>
    {
        private readonly IGroup<GameEntity> _lifetimeEntities;
        
        public ReduceLifetimeSystem(Contexts contexts) : base(contexts.input)
        {
            _lifetimeEntities = contexts.game.GetGroup(GameMatcher.Lifetime);
        }

        protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
        {
            return context.CreateCollector(InputMatcher.DeltaTime);
        }

        protected override bool Filter(InputEntity entity)
        {
            return entity.hasDeltaTime;
        }

        protected override void Execute(List<InputEntity> entities)
        {
            var deltaTime = entities[0].deltaTime.Value;
            foreach (var lifetimeEntity in _lifetimeEntities.GetEntities())
            {
                ApplyDeltaTime(lifetimeEntity, deltaTime);
            }
        }

        private void ApplyDeltaTime(GameEntity entity, fint deltaTime)
        {
            var currentLifetime = entity.lifetime.Value;
            var newLifetime = currentLifetime - deltaTime;
            if (newLifetime < fint.zero)
            {
                entity.isDestroyed = true;
            }
            else
            {
                entity.ReplaceLifetime(newLifetime);
            }
        }
    }
}
