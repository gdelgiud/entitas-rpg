﻿using Entitas;

namespace Game.Logic.Health
{
    [Game]
    public sealed class HealthComponent : IComponent
    {
        public int Value;
        public int Max;
    }
}
