﻿using System.Collections.Generic;
using Entitas;

namespace Game.Logic.Health
{
    public class ApplyDamageSystem : ReactiveSystem<GameEntity>, ICleanupSystem
    {
        private readonly GameContext _gameContext;
        private readonly IGroup<GameEntity> _damagedEntities;
        
        public ApplyDamageSystem(Contexts contexts) : base(contexts.game)
        {
            _gameContext = contexts.game;
            _damagedEntities = _gameContext.GetGroup(GameMatcher.Damaged);
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.DamageAction);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasDamageAction;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var action in entities)
            {
                ApplyDamage(action);
            }
        }

        private void ApplyDamage(GameEntity action)
        {
            var damageActionComponent = action.damageAction;
            var target = _gameContext.GetEntityWithId(damageActionComponent.TargetId);
            if (damageActionComponent.Amount > 0 && target != null && target.hasHealth)
            {
                var targetHealthComponent = target.health;
                var currentHealth = targetHealthComponent.Value;
                var newHealth = currentHealth - damageActionComponent.Amount;
                target.ReplaceHealth(newHealth, targetHealthComponent.Max);
                target.isDamaged = true;
            }
            action.isDestroyed = true;
        }

        public void Cleanup()
        {
            foreach (var damagedEntity in _damagedEntities.GetEntities())
            {
                damagedEntity.isDamaged = false;
            }
        }
    }
}
