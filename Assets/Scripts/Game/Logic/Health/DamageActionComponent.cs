﻿using Entitas;

namespace Game.Logic.Health
{
    [Game]
    public class DamageActionComponent : IComponent
    {
        public long TargetId;
        public int Amount;
    }
}
