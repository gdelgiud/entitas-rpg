﻿using DeterministicSimulation;
using Entitas;

namespace Game.Logic.Movement
{
    [Game]
    public sealed class VelocityComponent : IComponent
    {
        public fint X;
        public fint Y;
    }
}
