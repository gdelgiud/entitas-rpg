﻿using System.Collections.Generic;
using Entitas;

namespace Game.Logic.Movement
{
    public class MovementSystem : ReactiveSystem<InputEntity>
    {
        private readonly IGroup<GameEntity> _movableEntities;

        public MovementSystem(Contexts contexts) : base(contexts.input)
        {
            _movableEntities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Position, GameMatcher.Velocity));
        }

        protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
        {
            return context.CreateCollector(InputMatcher.DeltaTime);
        }

        protected override bool Filter(InputEntity entity)
        {
            return entity.hasDeltaTime;
        }

        protected override void Execute(List<InputEntity> entities)
        {
            var deltaTime = entities[0].deltaTime.Value;
            foreach (var entity in _movableEntities.GetEntities())
            {
                var positionComponent = entity.position;
                var velocityComponent = entity.velocity;
                entity.ReplacePosition(positionComponent.X + velocityComponent.X * deltaTime, positionComponent.Y + velocityComponent.Y * deltaTime);
            }
        }
    }
}
