﻿using Entitas;

namespace Game.Logic.Movement
{
    [Game]
    public sealed class DirectionComponent : IComponent
    {
        public Direction Value;
    }

    public enum Direction
    {
        Down,
        Left,
        Up,
        Right
    }
}
