﻿using System.Collections.Generic;
using DeterministicSimulation;
using Entitas;

namespace Game.Logic.Movement
{
    public class ComputeDirectionSystem : ReactiveSystem<GameEntity>
    {
        public ComputeDirectionSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Velocity);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasVelocity;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                ComputeEntityDirection(entity);
            }
        }

        private void ComputeEntityDirection(GameEntity entity)
        {
            var velocity = entity.velocity;
            var direction = GetCurrentDirection(entity);
            var xVelocity = velocity.X;
            var yVelocity = velocity.Y;
            var quadraticXVelocity = xVelocity * xVelocity;
            var quadraticYVelocity = yVelocity * yVelocity;
            if (quadraticXVelocity > quadraticYVelocity)
            {
                direction = GetHorizontalDirection(xVelocity);
            }
            else if (quadraticXVelocity < quadraticYVelocity)
            {
                direction = GetVerticalDirection(yVelocity);
            }
            else if (IsMoving(xVelocity, yVelocity))
            {
                direction = GetVerticalDirection(yVelocity);
            }
            entity.ReplaceDirection(direction);
        }

        private bool IsMoving(fint xVelocity, fint yVelocity)
        {
            return xVelocity != fint.zero || yVelocity != fint.zero;
        }

        private Direction GetCurrentDirection(GameEntity entity)
        {
            return entity.hasDirection ? entity.direction.Value : Direction.Down;
        }
        
        private Direction GetHorizontalDirection(fint xVelocity)
        {
            return xVelocity <= fint.zero ? Direction.Left : Direction.Right;
        }

        private Direction GetVerticalDirection(fint yVelocity)
        {
            return yVelocity <= fint.zero ? Direction.Down : Direction.Up;
        }
    }
}
