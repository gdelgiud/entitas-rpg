﻿using DeterministicSimulation;
using Entitas;
using Game.Extensions;

namespace Game.Logic
{
    public class MapCreationSystem : IInitializeSystem
    {
        private readonly GameContext _gameContext;
        
        public MapCreationSystem(Contexts contexts)
        {
            _gameContext = contexts.game;
        }
        
        public void Initialize()
        {
            var player = _gameContext.CreateCharacter("Character", fint.zero, fint.zero, 100);
            player.isControllable = true;

            var npc1 = _gameContext.CreateCharacter("Character", fint.one, fint.three, 50);
            var npc2 = _gameContext.CreateCharacter("Character", fint.CreateFromInt(-2), fint.CreateFromInt(2), 25);
            var tree1 = _gameContext.CreateTree(fint.one, fint.half);
        }
    }
}
