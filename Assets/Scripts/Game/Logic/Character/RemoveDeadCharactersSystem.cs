﻿using System.Collections.Generic;
using DeterministicSimulation;
using Entitas;

namespace Game.Logic.Character
{
    public class RemoveDeadCharactersSystem : ReactiveSystem<GameEntity>
    {
        public RemoveDeadCharactersSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Dead);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isDead;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var deadEntity in entities)
            {
                RemoveDeadEntity(deadEntity);
            }
        }

        private void RemoveDeadEntity(GameEntity deadEntity)
        {
            deadEntity.ReplaceLifetime(fint.half);
        }
    }
}
