﻿using Entitas;
using Game.Core.Destroy;
using Game.Input.Damage;
using Game.Input.Player;
using Game.Input.Time;
using Game.Logic;
using Game.Logic.Character;
using Game.Logic.Health;
using Game.Logic.Id;
using Game.Logic.Lifetime;
using Game.Logic.Movement;
using Game.View.Animation;
using Game.View.Asset;
using Game.View.Character;
using Game.View.Position;

namespace Game
{
    public sealed class ExampleSystems : Feature
    {
        public ExampleSystems(Contexts contexts)
        {
            contexts.game.OnEntityCreated += AddGameEntityId;
            
            // Initialization
            Add(new ResetNextIdSystem(contexts));
            Add(new MapCreationSystem(contexts));
            
            // Input emission
            Add(new DeltaTimeSystem(contexts));
            Add(new EmitPlayerMovementInputSystem(contexts));
            Add(new EmitEntityClickInputSystem(contexts));
            
            // Input processing
            Add(new ProcessPlayerMovementInputSystem(contexts));
            Add(new ProcessEntityClickedSystem(contexts));
            
            // Logic
            Add(new ReduceLifetimeSystem(contexts));
            Add(new MovementSystem(contexts));
            Add(new ComputeDirectionSystem(contexts));
            Add(new ApplyDamageSystem(contexts));
            Add(new DeathSystem(contexts));
            Add(new RemoveDeadCharactersSystem(contexts));
            
            // View
            Add(new LoadAssetSystem(contexts));
            Add(new AddAnimatorSystem(contexts));
            Add(new CharacterAnimationSystem(contexts));
            Add(new CharacterDamageFeedbackSystem(contexts));
            Add(new CharacterDeathEffectSystem(contexts));
            Add(new ViewPositioningSystem(contexts));
            Add(new UnloadAssetSystem(contexts));
            
            // Destroy
            Add(new DestroyGameEntitySystem(contexts));
            Add(new DestroyInputEntitySystem(contexts));
        }
        
        private void AddGameEntityId(IContext context, IEntity entity)
        {
            var gameContext = context as GameContext;
            var gameEntity = entity as GameEntity;

            if (gameContext == null || gameEntity == null || !gameContext.hasNextId) return;
        
            var nextId = gameContext.nextId.Value;
            gameEntity.ReplaceId(nextId);
            gameContext.ReplaceNextId(nextId + 1);
        }
    }
}
