﻿using DeterministicSimulation;
using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Game.Input.Time
{
    [Input]
    [Unique]
    public sealed class DeltaTimeComponent : IComponent
    {
        public fint Value;
    }
}
