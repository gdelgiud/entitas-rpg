﻿using DeterministicSimulation;
using Entitas;

namespace Game.Input.Time
{
    public class DeltaTimeSystem : IExecuteSystem
    {
        private readonly InputContext _inputContext;
        
        public DeltaTimeSystem(Contexts contexts)
        {
            _inputContext = contexts.input;
        }
        
        public void Execute()
        {
            _inputContext.ReplaceDeltaTime(fint.CreateFromFloat_Warning(UnityEngine.Time.deltaTime));
        }
    }
}
