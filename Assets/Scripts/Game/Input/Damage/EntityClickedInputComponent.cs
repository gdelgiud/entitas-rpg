﻿using Entitas;

namespace Game.Input.Damage
{
    [Input]
    public sealed class EntityClickedInputComponent : IComponent
    {
        public long EntityId;
    }
}
