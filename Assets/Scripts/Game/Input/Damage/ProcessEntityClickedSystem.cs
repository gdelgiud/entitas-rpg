﻿using System.Collections.Generic;
using Entitas;
using Game.Extensions;

namespace Game.Input.Damage
{
    public class ProcessEntityClickedSystem : ReactiveSystem<InputEntity>
    {
        private const int DamageAmount = 10;
        
        private readonly GameContext _gameContext;

        public ProcessEntityClickedSystem(Contexts contexts) : base(contexts.input)
        {
            _gameContext = contexts.game;
        }

        protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
        {
            return context.CreateCollector(InputMatcher.EntityClickedInput);
        }

        protected override bool Filter(InputEntity entity)
        {
            return entity.hasEntityClickedInput;
        }

        protected override void Execute(List<InputEntity> entities)
        {
            foreach (var inputEntity in entities)
            {
                ProcessEntityClicked(inputEntity);
            }
        }

        private void ProcessEntityClicked(InputEntity inputEntity)
        {
            var entityClicked = _gameContext.GetEntityWithId(inputEntity.entityClickedInput.EntityId);
            if (entityClicked != null)
            {
                _gameContext.CreateDamageAction(entityClicked, DamageAmount);
            }
            inputEntity.isDestroyed = true;
        }
    }
}
