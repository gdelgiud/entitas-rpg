﻿using Entitas;
using Entitas.Unity;
using Game.Extensions;
using UnityEngine;

namespace Game.Input.Damage
{
    public class EmitEntityClickInputSystem : IExecuteSystem
    {
        private readonly InputContext _inputContext;

        public EmitEntityClickInputSystem(Contexts contexts)
        {
            _inputContext = contexts.input;
        }

        public void Execute()
        {
            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                PerformRaycast();
            }
        }

        private void PerformRaycast()
        {
            var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition), Vector2.zero);
            if (hit.collider != null)
            {
                OnGameObjectClicked(hit.collider.gameObject);
            }
        }

        private void OnGameObjectClicked(GameObject gameObject)
        {
            var entityLink = gameObject.GetEntityLink();
            if (entityLink != null)
            {
                OnEntityClicked(entityLink.entity);
            }
        }

        private void OnEntityClicked(IEntity entity)
        {
            var gameEntity = entity as GameEntity;
            if (gameEntity != null && gameEntity.hasId)
            {
                _inputContext.OnEntityClicked(gameEntity);
            }
        }
    }
}
