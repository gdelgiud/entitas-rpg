﻿using DeterministicSimulation;
using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Game.Input.Player
{
    [Input]
    [Unique]
    public class PlayerMovementComponent : IComponent
    {
        public fint XAxis;
        public fint YAxis;
    }
}
