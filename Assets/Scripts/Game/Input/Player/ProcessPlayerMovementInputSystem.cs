﻿using Entitas;

namespace Game.Input.Player
{
    public class ProcessPlayerMovementInputSystem : IExecuteSystem
    {
        private readonly InputContext _inputContext;
        private readonly IGroup<GameEntity> _controllableEntities;
        
        public ProcessPlayerMovementInputSystem(Contexts contexts)
        {
            _inputContext = contexts.input;
            _controllableEntities = contexts.game.GetGroup(GameMatcher.Controllable);
        }

        public void Execute()
        {
            foreach (var controllableEntity in _controllableEntities.GetEntities())
            {
                if (_inputContext.hasPlayerMovement)
                {
                    ApplyMovementInput(controllableEntity, _inputContext.playerMovement);
                }
                else
                {
                    RemoveMovementInput(controllableEntity);
                }
            }
        }

        private void ApplyMovementInput(GameEntity controllableEntity, PlayerMovementComponent input)
        {
            controllableEntity.ReplaceVelocity(input.XAxis, input.YAxis);
        }

        private void RemoveMovementInput(GameEntity controllableEntity)
        {
            if (controllableEntity.hasVelocity)
            {
                controllableEntity.RemoveVelocity();
            }
        }
    }
}
