﻿using System;
using DeterministicSimulation;
using Entitas;

namespace Game.Input.Player
{
    public class EmitPlayerMovementInputSystem : IExecuteSystem
    {
        private const float Epsilon = 0.01f;
        
        private readonly InputContext _inputContext;

        public EmitPlayerMovementInputSystem(Contexts contexts)
        {
            _inputContext = contexts.input;
        }
        
        public void Execute()
        {
            var xAxis = UnityEngine.Input.GetAxis("Horizontal");
            var yAxis = UnityEngine.Input.GetAxis("Vertical");
            if (Math.Abs(xAxis) < Epsilon && Math.Abs(yAxis) < Epsilon)
            {
                ReleaseInput();
            }
            else
            {
                EmitInput(xAxis, yAxis);
            }
        }

        private void ReleaseInput()
        {
            if (_inputContext.hasPlayerMovement)
            {
                _inputContext.RemovePlayerMovement();
            }
        }

        private void EmitInput(float xAxis, float yAxis)
        {
            var emitedXAxis = fint.CreateFromFloat_Warning(xAxis);
            var emitedYAxis = fint.CreateFromFloat_Warning(yAxis);
            _inputContext.ReplacePlayerMovement(emitedXAxis, emitedYAxis);
        }
    }
}
