﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace Game.View.Position
{
    public class ViewPositioningSystem : ReactiveSystem<GameEntity>
    {
        public ViewPositioningSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.AllOf(GameMatcher.Position, GameMatcher.View));
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasPosition && entity.hasView;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                var position = entity.position;
                entity.view.GameObject.transform.position = new Vector3(position.X.ToFloat(), position.Y.ToFloat(), 0);
            }
        }
    }
}
