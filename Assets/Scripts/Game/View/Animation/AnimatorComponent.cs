﻿using Entitas;
using UnityEngine;

namespace Game.View.Animation
{
    [Game]
    public sealed class AnimatorComponent : IComponent
    {
        public Animator Animator;
    }
}
