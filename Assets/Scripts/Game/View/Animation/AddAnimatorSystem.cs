﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace Game.View.Animation
{
    public class AddAnimatorSystem : ReactiveSystem<GameEntity>
    {
        public AddAnimatorSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.View);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasView;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                AddAnimator(entity);
            }
        }

        private static void AddAnimator(GameEntity entity)
        {
            var animatorComponent = entity.view.GameObject.GetComponent<Animator>();
            if (animatorComponent != null)
            {
                entity.ReplaceAnimator(animatorComponent);
            }
        }
    }
}
