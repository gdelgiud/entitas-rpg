﻿using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;

namespace Game.View.Asset
{
    public class UnloadAssetSystem : ReactiveSystem<GameEntity>
    {
        public UnloadAssetSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return new Collector<GameEntity>(
                new[] {context.GetGroup(GameMatcher.Asset), context.GetGroup(GameMatcher.Destroyed)},
                new[] {GroupEvent.Removed, GroupEvent.Added});
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasView;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var e in entities)
            {
                DestroyView(e.view);
                e.RemoveView();
            }
        }

        private void DestroyView(ViewComponent viewComponent)
        {
            var gameObject = viewComponent.GameObject;
            gameObject.Unlink();
            Object.Destroy(gameObject);
        }
    }
}
