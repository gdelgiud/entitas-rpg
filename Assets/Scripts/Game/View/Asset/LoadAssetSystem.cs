﻿using System;
using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;

namespace Game.View.Asset
{
    public class LoadAssetSystem : ReactiveSystem<GameEntity>
    {
        private readonly GameContext _gameContext;

        public LoadAssetSystem(Contexts contexts) : base(contexts.game)
        {
            _gameContext = contexts.game;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Asset);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasAsset && !entity.hasView;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                var entity = entities[i];
                string assetName = entity.asset.Name;
                var asset = Resources.Load<GameObject>(assetName);
                GameObject gameObject = null;
                try
                {
                    gameObject = UnityEngine.Object.Instantiate(asset);
                }
                catch (Exception)
                {
                    Debug.Log("Cannot instantiate " + assetName);
                }

                if (gameObject != null)
                {
                    entity.AddView(gameObject);
                    gameObject.Link(entity, _gameContext);
                }
            }
        }
    }
}
