﻿using Entitas;

namespace Game.View.Asset
{
    [Game]
    public sealed class AssetComponent : IComponent
    {
        public string Name;
    }
}
