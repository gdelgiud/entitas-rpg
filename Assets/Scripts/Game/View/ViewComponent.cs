﻿using Entitas;
using UnityEngine;

namespace Game.View
{
    [Game]
    public sealed class ViewComponent : IComponent
    {
        public GameObject GameObject;
    }
}
