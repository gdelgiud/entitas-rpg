﻿using System.Collections.Generic;
using Entitas;
using Game.Extensions;

namespace Game.View.Character
{
    public class CharacterDeathEffectSystem : ReactiveSystem<GameEntity>
    {
        private const string DeathEffectName = "DeathBlood";
        private readonly GameContext _gameContext;
        
        public CharacterDeathEffectSystem(Contexts contexts) : base(contexts.game)
        {
            _gameContext = contexts.game;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Dead);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isDead && entity.hasPosition;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                ShowEntityDeathEffect(entity);
            }
        }

        private void ShowEntityDeathEffect(GameEntity entity)
        {
            _gameContext.CreateEffectOnEntity(entity, DeathEffectName);
        }
    }
}
