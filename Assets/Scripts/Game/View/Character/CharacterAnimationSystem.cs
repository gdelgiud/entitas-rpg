﻿using DeterministicSimulation;
using Entitas;
using Game.Logic.Movement;

namespace Game.View.Character
{
    public class CharacterAnimationSystem : IExecuteSystem
    {
        private readonly IGroup<GameEntity> _characterGroup;

        public CharacterAnimationSystem(Contexts contexts)
        {
            _characterGroup = contexts.game.GetGroup(GameMatcher.Animator);
        }

        public void Execute()
        {
            foreach (var character in _characterGroup.GetEntities())
            {
                Animate(character);
            }
        }

        private void Animate(GameEntity character)
        {
            var animator = character.animator.Animator;
            animator.SetBool("IsMoving", IsMoving(character));
            animator.SetInteger("Direction", GetDirection(character));
        }

        private int GetDirection(GameEntity character)
        {
            return (int) (character.hasDirection ? character.direction.Value : Direction.Down);
        }

        private bool IsMoving(GameEntity character)
        {
            return character.hasVelocity && (character.velocity.X != fint.zero || character.velocity.Y != fint.zero);
        }
    }
}
