﻿using System.Collections.Generic;
using Entitas;
using Game.Extensions;

namespace Game.View.Character
{
    public class CharacterDamageFeedbackSystem : ReactiveSystem<GameEntity>
    {
        private const string DamageFeedbackEffectName = "HitBlood";
        private readonly GameContext _gameContext;

        public CharacterDamageFeedbackSystem(Contexts contexts) : base(contexts.game)
        {
            _gameContext = contexts.game;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Damaged);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isDamaged && entity.hasPosition;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var damagedEntity in entities)
            {
                ShowDamageFeedback(damagedEntity);
            }
        }
        
        private void ShowDamageFeedback(GameEntity entity)
        {
            _gameContext.CreateEffectOnEntity(entity, DamageFeedbackEffectName);
        }
    }
}
