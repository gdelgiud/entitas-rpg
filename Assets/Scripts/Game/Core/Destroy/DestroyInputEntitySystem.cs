﻿using System.Collections.Generic;
using Entitas;

namespace Game.Core.Destroy
{
    public class DestroyInputEntitySystem : ReactiveSystem<InputEntity>
    {
        public DestroyInputEntitySystem(Contexts contexts) : base(contexts.input)
        {
        }

        protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
        {
            return context.CreateCollector(InputMatcher.Destroyed);
        }

        protected override bool Filter(InputEntity entity)
        {
            return entity.isDestroyed;
        }

        protected override void Execute(List<InputEntity> entities)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                var entity = entities[i];
                entity.Destroy();
            }
        }
    }
}
