﻿using System.Collections.Generic;
using Entitas;

namespace Game.Core.Destroy
{
    public class DestroyGameEntitySystem : ReactiveSystem<GameEntity>
    {
        public DestroyGameEntitySystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Destroyed);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isDestroyed;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                var entity = entities[i];
                entity.Destroy();
            }
        }
    }
}
