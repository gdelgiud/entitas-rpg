# Entitas RPG

A more complex [Entitas-CSharp](https://github.com/sschmid/Entitas-CSharp-Example) Unity project. Uses **Entitas 0.42.3**. See [Entitas Template](https://gitlab.com/gdelgiud/entitas-template) for a barebones template project.

Covers the following topics:
*  Loading Unity prefabs
*  Linking GameObjects to Entities
*  Receiving input (keyboard and clicks)
*  Processing said input, abstracting our game logic from Unity's input
*  Moving entities in a 2D world
*  Animating entities using Unity's Animator
*  Reactive and flexible gameplay
*  Reactive Ui
*  Unit testing game logic and presentation logic
